### What is this repository for? ###

* [four-forty 12S BMB](http://four-forty.com/product/12s-battery-monitoring-board/)


### How do I get set up? ###

* Download and install the [latest Arduino IDE](https://www.arduino.cc/en/Main/Software) (if it doesn't already exist on your system)
* Clone this repo to a local directory on your PC. Ensure you have the lastest master commit pulled 
* Navigate to src/four-forty_12S_BMB_Firmware/four-forty_12S_BMB_firmware.ino and double click to open in the Arduino IDE
* Connect an FTDI programmer to the header on the PCB, then connect to your computer (see the [user guide](http://four-forty.com/wp-content/uploads/2017/08/UserGuide_0.2.pdf) for further information)
* Select the appropriate COM port for your programmer
* Select board type as Arduino Nano (328p)
* Click upload and wait for programming to complete

### Contribution guidelines ###

* Contributions are welcomed, especially for users who can test and validate changes on hardware
* If you'd like to contribute, but don't have hardware, get in contact with the repo admin/s and we'll try to validate your contributions as we have time
* Hardware is currently only available to customers participating in the beta program, but design files are available for all products at four-forty.com
* Note: you are responsible for ensuring that you test and validate code in a safe manner. Please take precautions when testing experimental code or flashing dev builds

### Who do I talk to? ###

* Repo admin: foolsday01
* support@four-forty.com
