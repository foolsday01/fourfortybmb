#ifndef LTC6802_h
#define LTC6802_h

#include "Arduino.h"
#include "SPI.h"

#define WRCFG 0x01    //LTC Write Configuration Registers
#define RDCFG 0x02    //LTC Read config
#define RDCV 0x04     //LTC Read cells
#define STCVAD 0x10   //LTC Start all A/D's - poll status
#define RDFLG 0x06    //LTC Read Flags
#define RDTMP 0x08    //LTC Read Temperatures
#define STCDC 0x60    //LTC A/D converter and poll Status
#define STOWAD 0x20   //LTC Start Test - poll status
#define STTMPAD 0x30  //LTC Temperature Reading - ALL
#define INTTEMP 0x33  //LTC Internal temperature conversion trigger

class LTC6802{

public:
  LTC6802();
  LTC6802(int chipSelectPin, int tempChipAddress);
  void config();
  void convert(); // REVIEW: add number of cells option?
  void bleedEnable(int cellNumber);
  void bleedDisable(int cellNumber);
  void bleedDisableAll();
  void LED(boolean state);
  int chipRev();
  int readBleedState();
  double temperature(int NTC_number);
  double cellVoltage(int cellNumber);

private:
  void requestTemps();
  void requestVoltages();
  int csPin;
  int chipAddress;
  double voltArray[12];
  double tempArray[3];

};

#endif
