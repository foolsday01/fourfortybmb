#include "LTC6802.h"

//Static functions common to all instances

static void spiSetup(){
  SPI.setBitOrder(MSBFIRST); //Most sig. bit first
  SPI.setDataMode(SPI_MODE3); //Sends and requests 2 bytes at a time
  SPI.setClockDivider(SPI_CLOCK_DIV32); //Normally div 32
  SPI.begin();
  pinMode(A0,OUTPUT);
  delay(20);
}

static byte noBlockSPItransfer(byte _data) {
  SPDR = _data;
  int startMillis = 0;
  startMillis = millis();
  while (millis()-startMillis < 100)
    {
    if (SPSR & _BV(SPIF))
       break;
    }
  return SPDR;
}

//End of static functions

LTC6802::LTC6802(){ //TODO: Remove, just use the 2 param constructor with nonsense values until real ones are determined
  csPin = 100; //Fake value
  chipAddress = 256; //Fake value
}

LTC6802::LTC6802(int chipSelectPin, int tempChipAddress){
  csPin = chipSelectPin;
  chipAddress = tempChipAddress;
  pinMode(csPin,OUTPUT); //TODO: move pin definitions and digitalWrites outside constructor to config (undefined state at startup)
  digitalWrite(csPin, HIGH);
}

void LTC6802::config(){
  spiSetup();
  digitalWrite(csPin, LOW); //Trigger LTC chip select
  SPI.transfer(chipAddress);
  SPI.transfer(WRCFG); //Command to write to configuration registers
  SPI.transfer(B01100001);//0 Set comparator duty cycle to default
  SPI.transfer(0x00);//1 Don't trigger ANY cell balancing
  SPI.transfer(0x00);//2 Enable interrupts for cells 1-4
  SPI.transfer(0x00);//3 Enable interrupts for cells 5-12
  SPI.transfer(0x71);//4 2.712V low voltage reference point (not currently used)
  SPI.transfer(0xAB);//5 4.104V over voltage reference points (not currently used)
  digitalWrite(csPin, HIGH); //End write
}

void LTC6802::convert(){

  digitalWrite(csPin,LOW);
  //SPI.transfer(chipAddress); //If conversion is just desired on one board
  SPI.transfer(STCVAD); //LTC Start all A/D's - poll status (all cells)
  delay(13); // wait at least 12ms as per data sheet, p.24
  digitalWrite(csPin,HIGH); //Exit polling
  requestVoltages();

  digitalWrite(csPin,LOW);
  SPI.transfer(STTMPAD);
  delay(20); //REVIEW: is this neccesary?
  digitalWrite(csPin,HIGH); //Trigger temp ADC conversion
  requestTemps();
}

void LTC6802::bleedEnable(int cellNumber){ //TODO: fix masking?

  int balanceMask = 0;
  balanceMask = round(pow(2.0,float(cellNumber-1))); //Creates a 1.5 byte number
  int regTemp[6];

  int lsMask = balanceMask & 0x0FF;
  int msMask = (balanceMask & 0xF00)>>8; //split it into low and high bytes

  digitalWrite(csPin, LOW); //Read the old config
  SPI.transfer(chipAddress);
  SPI.transfer(RDCFG); //Command to read from configuration registers
  for(int i = 0; i < 6; i++) //Cycle through each all config registers
  {
   regTemp[i] = SPI.transfer(RDCFG);
  }
  digitalWrite(csPin, HIGH); //Release LTC chip select

  int newReg1 = (regTemp[1] | lsMask);
  int newReg2 = (regTemp[2] | msMask);

  digitalWrite(csPin, LOW); //Trigger LTC chip select
  SPI.transfer(chipAddress);
  SPI.transfer(WRCFG); //Command to write to configuration registers
  SPI.transfer(regTemp[0]);//0 Set comparator duty cycle to default
  SPI.transfer(newReg1);//1 Don't trigger ANY cell balancing
  SPI.transfer(newReg2);//2 Enable interrupts for cells 1-4
  SPI.transfer(0x00);//3 Enable interrupts for cells 5-12
  SPI.transfer(0x71);//4 2.712V low voltage reference point (not currently used)
  SPI.transfer(0xAB);//5 4.104V over voltage reference points (not currently used)
  digitalWrite(csPin, HIGH); //End write
}

void LTC6802::bleedDisable(int cellNumber){
  int balanceMask = round(pow(2.0,float(cellNumber-1))); //Creates a 1.5 byte number
  int regTemp[6];
  balanceMask = ~balanceMask; //invert in preparation for or-ing

  int lsMask = balanceMask & 0x0FF;
  int msMask = (balanceMask & 0xF00)>>8; //split it into low and high bytes

  digitalWrite(csPin, LOW); //Read the old config
  SPI.transfer(chipAddress);
  SPI.transfer(RDCFG); //Command to read from configuration registers
  for(int i = 0; i < 6; i++) //Cycle through each all config registers
  {
   regTemp[i] = SPI.transfer(RDCFG);
  }
  digitalWrite(csPin, HIGH); //Release LTC chip select

  int newReg1 = (regTemp[1] & lsMask);
  int newReg2 = (regTemp[2] & msMask);

  digitalWrite(csPin, LOW); //Trigger LTC chip select
  SPI.transfer(chipAddress);
  SPI.transfer(WRCFG); //Command to write to configuration registers
  SPI.transfer(regTemp[0]);//0 Set comparator duty cycle to default
  SPI.transfer(newReg1);//1
  SPI.transfer(newReg2);//2
  SPI.transfer(0x00);//3 Enable interrupts for cells 5-12
  SPI.transfer(0x71);//4 2.712V low voltage reference point (not currently used)
  SPI.transfer(0xAB);//5 4.104V over voltage reference points (not currently used)
  digitalWrite(csPin, HIGH); //End write
}

void LTC6802::bleedDisableAll(){
  int regTemp[6];
  digitalWrite(csPin, LOW); //Read the old config
  SPI.transfer(chipAddress);
  SPI.transfer(RDCFG); //Command to read from configuration registers
  for(int i = 0; i < 6; i++) //Cycle through each all config registers
  {
   regTemp[i] = SPI.transfer(RDCFG);
  }
  digitalWrite(csPin, HIGH); //Release LTC chip select
  regTemp[1] = 0x00; //No balancing
  regTemp[2] = 0x00; //No balancing

  digitalWrite(csPin, LOW); //Trigger LTC chip select
  SPI.transfer(chipAddress);
  SPI.transfer(WRCFG); //Command to write to configuration registers

  for(int i = 0;i<6;i++){
    SPI.transfer(regTemp[i]);
  }

  digitalWrite(csPin, HIGH); //End write
}

int LTC6802::readBleedState(){ //TODO: Not working yet
  int regTemp[6];
  digitalWrite(csPin, LOW); //Read the old config
  SPI.transfer(chipAddress);
  SPI.transfer(RDCFG); //Command to read from configuration registers
  for(int i = 0; i < 6; i++) //Cycle through each all config registers
  {
   regTemp[i] = SPI.transfer(RDCFG);
  }
  digitalWrite(csPin, HIGH); //Release LTC chip select
  return  (regTemp[1]<<8 || regTemp[2]);
}

double LTC6802::temperature(int NTC_number){
  return tempArray[NTC_number];
}

double LTC6802::cellVoltage(int cellNumber){
  return voltArray[cellNumber-1];
}

void LTC6802::requestVoltages(){
  int cellVoltage = 0.0; //default value
  int cellVoltagesRaw[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //holds raw cell array
  digitalWrite(csPin,LOW); //Trigger LTC chip select
  SPI.transfer(chipAddress); //Initiate transaction with LTC
  SPI.transfer(RDCV); //Command to read cell voltage register group
  //Serial.println("Starting");
  for(int j = 0; j<18;j++) //Cycle through the 18 registers
  {
    cellVoltagesRaw[j] = SPI.transfer(RDCV);
  }
  digitalWrite(csPin,HIGH); //Release LTC chip select
  delay(60); //REVIEW: how long does this actually need to be?
  for(int j = 1; j<13; j++){
    int index1 = (j*1.5-1.5);
    int index2 = (j*1.5-.5);
    int index3 = (j*1.5-2);
    int index4 = (j*1.5-1);

    if(j % 2){ //if odd cell#
      voltArray[j-1] = (((cellVoltagesRaw[index1] & 0xFF) | ((cellVoltagesRaw[index2] & 0x0F) << 8))*.0015);
    }else{
      voltArray[j-1] = ((((cellVoltagesRaw[index3] & 0xF0) >> 4) | ((cellVoltagesRaw[index4] & 0xFF) << 4 ))*.0015);
    }
  }
}

void LTC6802::requestTemps(){ //DEBUG: voltage still not quite right
  int temp[5]; //Temporary array to hold temperature register data
  digitalWrite(csPin,LOW);
  SPI.transfer(chipAddress);
  SPI.transfer(RDTMP);
  delay(5);
  for(int i = 0; i<5;i++){
   temp[i] = SPI.transfer(RDTMP);
  }
  digitalWrite(csPin,HIGH);

  tempArray[1] = ((temp[0]) | (temp[1] & 0x0F)<<8)*1.5; //1.569; //Voltage present at pin (mV)
  tempArray[2] = (((temp[1] & 0xF0)>>4) | ((temp[2])<<4))*1.5; //1.569; //Voltage present at pin (mV)
  tempArray[0] = ((temp[3]) | ((temp[4] & 0x0F))<<8) * .1875 - 273.15; //die temp in C

}

int LTC6802::chipRev(){
  int TMPR4 = 0;
  int REV = 0;

  SPI.transfer(chipAddress);
  SPI.transfer(RDTMP);

  for(int i = 0; i<5; i++){
    TMPR4 = SPI.transfer(RDTMP);
  }
  digitalWrite(csPin,HIGH);

  REV = TMPR4>>5;
  return REV;
}

void LTC6802::LED(boolean state){
  int regTemp[6];

  digitalWrite(csPin, LOW); //Read the old config
  SPI.transfer(chipAddress);
  SPI.transfer(RDCFG); //Command to read from configuration registers
  for(int i = 0; i < 6; i++) //Cycle through each all config registers
  {
   regTemp[i] = SPI.transfer(RDCFG);
  }
  digitalWrite(csPin, HIGH); //Release LTC chip select
  digitalWrite(csPin, LOW); //Trigger LTC chip select
  SPI.transfer(chipAddress);
  SPI.transfer(WRCFG); //Command to write to configuration registers
  if(state){
    SPI.transfer(B01100001);//
  }else{
    SPI.transfer(B00000001);//
  }
  for (int i = 1;i<6;i++){
    SPI.transfer(regTemp[i]);
  }
  //SPI.transfer(0x00);//1 Don't trigger ANY cell balancing
  //SPI.transfer(0x00);//2 Enable interrupts for cells 1-4
  //SPI.transfer(0x00);//3 Enable interrupts for cells 5-12
  //SPI.transfer(0x71);//4 2.712V low voltage reference point (not currently used)
  //SPI.transfer(0xAB);//5 4.104V over voltage reference points (not currently used)
  digitalWrite(csPin, HIGH); //End write
}
