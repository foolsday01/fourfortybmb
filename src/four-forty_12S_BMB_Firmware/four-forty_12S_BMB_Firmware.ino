#include <Wire.h>
#include <EEPROM.h>
#include "LTC6802.h"
#include "Configuration.h"
#include "bmbTimer.h"

LTC6802 bmb[maxBoards] = LTC6802(); //REVIEW 3% dynamic mem used by each board object
bmbTimer balanceTimer = bmbTimer(balancePeriod);

void setup() {

  if (serialEnabled){
    Serial.begin(9600);
    Serial.println("--four-forty BMB--");
    Serial.println("support@four-forty.com");
    Serial.print("Firmware version: ");
    Serial.print(firmwareMajorVersion);
    Serial.print('.');
    Serial.print(firmwareMinorVersion);
    Serial.println(" (BETA)"); //TODO: remove for release
}

  if (I2C_enabled){
    Wire.begin(I2C_address); //Join I2C bus as defined address
    Wire.onReceive(receiveEvent); //Handles I2C writes
    Wire.onRequest(requestEvent); //Handles I2C reads
    if(serialEnabled){
    Serial.println("I2C initialized");
  }
}

  pingWatchdog(); //Reset watchdog due to setup time REVIEW: is this neccesary?

  retrieveEEPROM(); //Function to pull params out of EEPROM and assign to globals

  pinMode(contactor1Pin,OUTPUT);
  pinMode(contactor2Pin,OUTPUT);
  pinMode(ledPin,OUTPUT);

  digitalWrite(contactor1Pin,LOW);
  digitalWrite(contactor2Pin,LOW);
  digitalWrite(ledPin,HIGH);

  initializeChips(); //Finds board present on the bus, and initializes them + their objects
  startupBlink(); //Does a blink pattern on all the boards it finds

  balanceTimer.start();
}

void loop() {

  if(broadcastOn){
    broadcast();
  }

  if(ALERT){ //Lights LED on the logic side of the board if there's currently an alert
    digitalWrite(ledPin,HIGH);
  }else{
    digitalWrite(ledPin, LOW);
  }

  for (byte board = 0;board<maxBoards;board++){
    if(foundBoards[board]){ //Don't waste time talking to boards that don't exist
      bmb[board].convert(); //Trigger ADC conversion of latest voltages and temperatures
      checkForVoltageFaults(board,boardSCount[board]); //Check for voltage related faults
      checkForTemperatureFaults(board); //Check for temperature related faults
    }
      pingWatchdog(); //Every time it finishes checking a board
  }
    if (balanceTimer.isItTime()){

      if(actuallyBalancing){ //Control LED indicator
        for(int board = 0;board<maxBoards;board++){
          if (foundBoards[board]){
              bmb[board].LED(true);
          }
        }
      }else{
        for(int board = 0;board<maxBoards;board++){
          if (foundBoards[board]){
              bmb[board].LED(false);
          }
        }
      }

      if(autoBalance && (!BRICK_UNDERVOLTAGE)){
        actuallyBalancing = false; //Set this false. Will be switched to true if anything changes in balanceState
        for(int board = 0;board<maxBoards;board++){
          if (foundBoards[board]){
              balanceState(board,boardSCount[board]); //Balances board if criteria is met TODO: modify s-count
          }
        }
      }else{
        for(int board = 0;board<maxBoards;board++){
          if (foundBoards[board]){
            bmb[board].bleedDisableAll(); //Turns off all cell balancing
          }
          actuallyBalancing = false; //Now that all boards are off, set actuallyBalancing to off
        }
      }
    }

  if(serialEnabled){ //If serial is enabled, check buffer for requests
    serialCheck();
    serialParser();
    }

  setAlert();
}

void checkForVoltageFaults(int boardNum, int sCount){

  double maxCellVoltage = 0.0; //Choose unreasonably low voltage as default
  double minCellVoltage = 5.0; //Choose unreasonably high voltage as default

  for (int cell = 0; cell < sCount; cell++){ //Finds the highest and lowest brick voltage
    double tempVoltage = bmb[boardNum].cellVoltage(cell+1);
    if(tempVoltage>maxCellVoltage){maxCellVoltage = tempVoltage;}
    if(tempVoltage<minCellVoltage){minCellVoltage = tempVoltage;}
  }

  //---VOLTAGE FAULT CHECKS---
  if (maxCellVoltage >= maxVoltage){ //Check for overvoltage
    BRICK_OVERVOLTAGE = true;
  }else{
    BRICK_OVERVOLTAGE = false;
  }
  if (minCellVoltage <= minVoltage){ //Check for undervoltage
    BRICK_UNDERVOLTAGE = true;
  }else{
    BRICK_UNDERVOLTAGE = false;
  }
  if ((maxCellVoltage-minCellVoltage)>= maxBalanceDifference){ //Check for excessive imbalance
    UNACCEPTABLE_IMBALANCE = true;
  }else{
    UNACCEPTABLE_IMBALANCE = false;
  }
}

void checkForTemperatureFaults(int boardNum){
  double NTC_1_Temp = B57540G0104J000(int(bmb[boardNum].temperature(1)));
  double NTC_2_Temp = B57540G0104J000(int(bmb[boardNum].temperature(2)));
  double dieTemp = bmb[boardNum].temperature(0);

//---TEMPERATURE FAULT CHECKS---

    if((enableExtTemp == 1) || (enableExtTemp == 3)){
      if (NTC_1_Temp >= maxExtTemp){ //NTC1
        NTC1_OVERTEMP = true;
      }else{
        NTC1_OVERTEMP = false;
      }
      if (NTC_1_Temp <= minExtTemp){
        NTC1_UNDERTEMP = true;
      }else{
        NTC1_UNDERTEMP = false;
      }
    }else{
      NTC1_UNDERTEMP = false;
      NTC1_OVERTEMP = false;
    }
    if((enableExtTemp == 2) || (enableExtTemp == 3)){
      if (NTC_2_Temp >= maxExtTemp){ //NTC2
        NTC2_OVERTEMP = true;
      }else{
        NTC2_OVERTEMP = false;
      }
      if (NTC_2_Temp <= minExtTemp){
        NTC2_UNDERTEMP = true;
      }else{
        NTC2_UNDERTEMP = false;
      }
    }else{
      NTC2_UNDERTEMP = false;
      NTC2_OVERTEMP = false;
    }

//---DIE TEMPERATURE---
    if (dieTemp >= maxDieTemp){ //General fault checks
      DIE_OVERTEMP = true;
    }else{
      DIE_OVERTEMP = false;
    }
}

void balanceState(int boardNumber, int sCount){

  bmb[boardNumber].bleedDisableAll(); //Turns off all cell balancing

  double minCellVoltage = 5.0; //Choose max voltage as the reference
  double tempVoltage[sCount];
  int balanceArray[sCount+1]; //Counts from 1-sCount (does not use 0th index)

  for (int index = 0; index < sCount; index++){ //Find the lowest cell voltage
    tempVoltage[index] = bmb[boardNumber].cellVoltage(index+1);
    if((tempVoltage[index]<minCellVoltage)  && (tempVoltage[index]>=minVoltage)){
      minCellVoltage = tempVoltage[index];
    }
  }

  for(int index = 0; index < sCount; index++){ //Compare each cell to min, determine if it needs discharge
    if((tempVoltage[index]-minCellVoltage)>targetBalanceDifference){
      balanceArray[index+1] = true;
      actuallyBalancing = true;
    }else{
      balanceArray[index+1] = false;
    }
  }

  if(checkerBoard){ //if checkerBoard is true, balance evens that need it
    for(int cell = 1; cell < sCount+1; cell=cell+2){
      if(balanceArray[cell]){
        bmb[boardNumber].bleedEnable(cell); //TODO: make new method to take state of all 12 FETs at once
      }
    }
  }else{ //if checkerBoard is false, balance odds that need it
    for(int cell = 2; cell < sCount+1; cell=cell+2){
      if(balanceArray[cell]){
        bmb[boardNumber].bleedEnable(cell);
      }
    }
  }
  checkerBoard = !checkerBoard; //Alternate the state for next time
}

void pingWatchdog(){
  digitalWrite(watchdogPin,HIGH); //Processor watchdog
  digitalWrite(LTC_watchdogPin,LOW); //LTC watchdog (SCK line)
  delay(1); //TODO: check if a delay is needed
  digitalWrite(watchdogPin,LOW);
  digitalWrite(LTC_watchdogPin,HIGH);
}

void initializeChips(){
  //Cycles through all 16 possible addresses, create temporary LTC6802 object, config and convert the board,
  // turn off the LED, and verify die temp reading as a check to see if board is real
  for (int i = 0; i < 16; i++){
    int address = 0;
    address = 128+i;
    bmb[i] = LTC6802(chipSelectPin,address);
    bmb[i].config();
    bmb[i].convert();
    bmb[i].LED(0);
    if(bmb[i].temperature(0)<maxDieTemp && bmb[i].temperature(0)>dieTempMin){ //If the chip responds with an accept temp, mark it as found
      foundBoards[i] = true;
    }else{
      foundBoards[i] = false;
    }
  }
}

void startupBlink(){
  //Turns on LEDs on discovered boards one at a time, then turns them off one at a time again
  for(int i = 0;i<maxBoards;i++){
    if(foundBoards[i]){
      bmb[i].LED(0);
    }
  }
  for(int i = 0;i<maxBoards;i++){
    if(foundBoards[i]){
      bmb[i].LED(1);
    }
  }
  for(int i = 0;i<maxBoards;i++){
    if(foundBoards[i]){
      bmb[i].LED(0);
    }
  }
}

void interfaceLogic(byte interface){ //Reads message from master, returns answer over I2C or serial depending on if interface is 0 or 1 respectively
  if(masterMessage[0] >= 124){ //Special command
    switch(masterMessage[0]){
      case 128: //Command byte write
        EEPROM.update(0,1); //Value has been changed, unlock alarm mode if not already
        EEPROM.update(masterMessage[1],masterMessage[2]);
        if(interface){
          Serial.println("Done");
        }
        break;
      case 127: //Command byte read
        if (!interface){
          interfaceResponse[0] = EEPROM.read(masterMessage[1]);
          validBytes = 1;
        }else{
          Serial.println(EEPROM.read(masterMessage[1]));
        }
        break;
      case 126: //Connected boards query
        if (!interface){
          interfaceResponse[0] = boardsConnected();
          validBytes = 1;
        }else{
          Serial.println(boardsConnected());
        }
        break;
      case 125: //ALERT
      if (!interface){
        interfaceResponse[0] = alertCode;
        validBytes = 1;
      }else{
        Serial.println(alertCode);
      }
        break;
      case 124: //Board Command
        switch(masterMessage[1]){
          case 1: //EnableBalancing
            autoBalance = masterMessage[2];
            if(interface){ //If serial mode
              if(masterMessage[2]>0){
                Serial.println("Auto balancing: Enabled");
              }else{
                Serial.println("Auto balancing: Disabled");
              }
            }
          break;
          case 2: //ChargeMode [FUTURE FEATURE]
            if(interface){
              Serial.println("Featured not yet enabled");
            }
          break;
          case 3: //Chip LED [FUTURE FEATURE]
            if(interface){
              Serial.println("Featured not yet enabled");
            }
          break;
          case 4: //Enable digital output 1
            if(masterMessage[2]==1){
              digitalWrite(contactor1Pin,HIGH);
              if(interface){
                Serial.println("Digital pin 1: HIGH");
              }
            }else if(masterMessage[2]==0){
              digitalWrite(contactor1Pin,LOW);
              if(interface){
                Serial.println("Digital pin 2: LOW");
              }
            }else{
                if(interface){
                  Serial.println("INVALID COMMAND (Must be 1 or 0!)");
                }
            }
          break;
          case 5: //Enable digital output 2 (only if enableAlertLine is false)
            if(!enableAlertLine){
              if(masterMessage[2]==1){
                digitalWrite(contactor2Pin,HIGH);
                if(interface){
                  Serial.println("Digital pin 2: HIGH");
                }
              }else if(masterMessage[2]==0){
                digitalWrite(contactor2Pin,LOW);
                if(interface){
                  Serial.println("Digital pin 2: LOW");
                }
              }else{
                  if(interface){
                    Serial.println("INVALID COMMAND (Must be 1 or 0!)");
                  }
              }
            }else{
              if(interface){
                Serial.println("INVALID COMMAND (disable enableAlertLine first!)");
              }
            }
          break;
        }
      break;
    }
  }else if(masterMessage[0]>0){ //Assume that this is a board value (non 1-(max board count) will be undefined)
    if(masterMessage[1]<1){
      //Return the undefined case (validBytes = 0)
        if(interface){
          Serial.println("INVALID COMMAND (Cells are indexed starting at 1!)");
        }
    }else if(masterMessage[1]<=12){
      //Requesting cell voltage
      int tempVoltage = 0;
      tempVoltage = int(bmb[masterMessage[0]-1].cellVoltage(masterMessage[1])*1000);
      if (!interface){
        interfaceResponse[0] = byte(tempVoltage & 0x00FF); //LSB of 13 bit value
        interfaceResponse[1] = byte((tempVoltage & 0xFF00) >> 8); //MSB (or 5 bits) of 13 bitvalue
        validBytes = 2;
      }else{
        Serial.println(tempVoltage);
      }

    }else if(masterMessage[1]<=15){
      //Looking for some raw temperature
      switch(masterMessage[1]){
        case 13: //dieTemp
        if (!interface){
          interfaceResponse[0] = int(bmb[masterMessage[0]-1].temperature(0));
        }else{
          Serial.println(bmb[masterMessage[0]-1].temperature(0));
        }
        break;
        case 14: //ext1
        if (!interface){
          interfaceResponse[0] = int(bmb[masterMessage[0]-1].temperature(1));
        }else{
          Serial.println(bmb[masterMessage[0]-1].temperature(1));
        }
        break;
        case 15: //ext2
        if (!interface){
          interfaceResponse[0] = int(bmb[masterMessage[0]-1].temperature(2));
        }else{
          Serial.println(bmb[masterMessage[0]-1].temperature(2));
        }
        break;
      }
      if (!interface){
        validBytes = 1;
      }
    }else if(masterMessage[1]<=19){
      //Looking for a processed temperature
      switch(masterMessage[1]){
        case 17: // Processed thermistor 1 [C]
          Serial.println("Processed temp 1:");
          if (!interface){
            if(thermistorMap == 0){
              interfaceResponse[0] = temperatureLookup(int(bmb[masterMessage[0]-1].temperature(1)));
            }
          }else{
            Serial.println(temperatureLookup(int(bmb[masterMessage[0]-1].temperature(1))));
          }
        break;
        case 18: // Processed thermistor 2 [C]
          Serial.println("Processed temp 2:");
          if (!interface){
            if(thermistorMap == 0){
              interfaceResponse[0] = temperatureLookup(int(bmb[masterMessage[0]-1].temperature(2)));
            }
          }else{
            Serial.println(temperatureLookup(int(bmb[masterMessage[0]-1].temperature(2))));
          }
        break;
      }

      if (!interface){
        validBytes = 1;
      }
    }else{
      //Return the undefined case
      Serial.println("Featured not yet enabled");
    }
  }else{ //An undefined value has been sent in the first masterMessage position
    Serial.println("INVALID COMMAND");
  }
  memset(masterMessage,0,sizeof(masterMessage)); //Clear it to avoid repeated values
}

void receiveEvent(int howMany){ //I2C interrupt for master writes
  char index = 0;

  while(Wire.available() > 0){
    masterMessage[index] = Wire.read();
    index++;
  }

  interfaceLogic(0); //Check what has been received

  for(char i=0;i<3;i++){
    masterMessage[i] = 0; //Just to be safe, clear out the old message
  }
}

void requestEvent(){ //I2C interrupt for master reads
  for(int i=0;i<validBytes;i++){
  Wire.write(interfaceResponse[i]);
  }
}

byte boardsConnected(){
  byte boards = 0;
  for(int i = 0;i<maxBoards;i++){
    boards += foundBoards[i];
  }
  return boards;
}

void retrieveEEPROM(){
  settingsModified = EEPROM.read(0); //Gets set to 1 once a parameter has been changed
  maxVoltage = (float(EEPROM.read(1)))+(float(EEPROM.read(2))/100.0);
  minVoltage = (float(EEPROM.read(3)))+(float(EEPROM.read(4))/100.0);
  boardSCount[0] = EEPROM.read(5);
  boardSCount[1] = EEPROM.read(6);
  boardSCount[2] = EEPROM.read(7);
  boardSCount[3] = EEPROM.read(8);
  boardSCount[4] = EEPROM.read(9);
  boardSCount[5] = EEPROM.read(10);
  boardSCount[6] = EEPROM.read(11);
  boardSCount[7] = EEPROM.read(12);
  maxExtTemp = EEPROM.read(13)-128;
  minExtTemp = EEPROM.read(14)-128;
  thermistorMap = EEPROM.read(15);
  maxDieTemp = EEPROM.read(16)-128;
  warningDieTemp = EEPROM.read(17)-128;
  targetBalanceDifference = float(EEPROM.read(18))/1000.0;
  maxBalanceDifference = float(EEPROM.read(19))/1000.0;
  autoBalance = EEPROM.read(20);
  serialEnabled = EEPROM.read(21);
  balanceLED = EEPROM.read(22);
  enableExtTemp = EEPROM.read(23);
  enableAlertLine = EEPROM.read(24);
}

void serialCheck(){ //Structure based off: http://forum.arduino.cc/index.php?topic=396450
  static byte index = 0;
  char newByte;
  while(Serial.available() > 0 && newData == false){
      newByte = Serial.read();
      if((newByte != endMarker1) && (newByte != endMarker2)){
        receivedChars[index] = newByte;
        index++;
        if(index >= maxChars){
          index = maxChars-1 ; //Prevent errors if a large message is sent
        }
      }else{
        receivedChars[index] = '\0'; //Null termination for char array
        index = 0;
        newData = true;
      }
  }
}

void serialParser(){
    byte startPoint = 0;
    byte commandCount = 0;
    if(newData){
      for (byte i = 0; i<=strlen(receivedChars); i++){
        if((receivedChars[i]==' ') || (i==strlen(receivedChars))){
          char command[i-startPoint+1]; //Create a new char array of the correct length
          command[i-startPoint] = '\0'; //Null terminate the new char array
          byte k = 0;
          for(byte j = startPoint; j<i; j++){
            command[k] = receivedChars[j];
            k++;
          }
          startPoint = i+1;
          masterMessage[commandCount] = commandLookup(command);
          commandCount++;
        }
      }
      interfaceLogic(1);
      newData = false;
    }
}

int commandLookup(char* command){
  if ((!strcmp(command,"WRT")) || (!strcmp(command,"wrt"))){
    return 128;
  }else if ((!strcmp(command,"RD")) || (!strcmp(command,"rd"))){
      return 127;
  }else if ((!strcmp(command,"QCONN")) || (!strcmp(command,"qconn"))){
      return 126;
  }else if ((!strcmp(command,"QALERT")) || (!strcmp(command,"qalert"))){
      return 125;
  }else if ((!strcmp(command,"COM")) || (!strcmp(command,"com"))){
      return 124;
  }else{
    boolean isValid = true;

    for (byte i = 0; i<strlen(command); i++){ //Check through string to see if it has any alpha characters
      if (isalpha(command[i])){
        isValid = false;
      }
    }
    if (isValid){
      return atoi(command); //Char array to int
    }else{
      return 0; //Invalid command
    }
  }
}

void setAlert(){
  if(BRICK_OVERVOLTAGE || BRICK_UNDERVOLTAGE || UNACCEPTABLE_IMBALANCE || NTC1_OVERTEMP || NTC1_UNDERTEMP || NTC2_OVERTEMP || NTC1_UNDERTEMP || DIE_OVERTEMP|| (boardsConnected()<1)){
    ALERT = true;
    if(enableAlertLine){
      digitalWrite(contactor2Pin, HIGH);
    }
  }else{
    ALERT = false;
    if(enableAlertLine){
      digitalWrite(contactor2Pin, LOW);
    }
  }

  if(BRICK_OVERVOLTAGE){
    alertCode = 1;
  }else if(NTC1_OVERTEMP || NTC2_OVERTEMP){
    alertCode = 2;
  }else if(BRICK_UNDERVOLTAGE){
    alertCode = 3;
  }else if(NTC1_UNDERTEMP || NTC2_UNDERTEMP){
    alertCode = 4;
  }else if(UNACCEPTABLE_IMBALANCE){
    alertCode = 5;
  }else if(DIE_OVERTEMP){
    alertCode = 6;
  }else if(boardsConnected()<1){
    alertCode = 7;
  }else{
    alertCode = 0;
  }
}

byte B57540G0104J000(int ADC_voltage){
  byte temperature;
  if(ADC_voltage > 4652){
    temperature = -25;
  }else if(ADC_voltage > 3605){
    temperature = byte((float(ADC_voltage) * -0.0307) + 119.23);
  }else if(ADC_voltage > 838){
    temperature = byte((float(ADC_voltage) * -0.0202) + 76.51);
  }else if(ADC_voltage > 299){
    temperature = byte((float(ADC_voltage) * -0.0641) + 116.36);
  }else if(ADC_voltage >= 0){
    temperature = 100;
  }
  return temperature;
}

byte temperatureLookup(int ADC_voltage){
  byte temperature;
  switch(thermistorMap){
    case 0:
      for(byte i = 0; i<26; i++){
         int tempVoltage = pgm_read_word_near(map_0 + i);
         if(ADC_voltage >= tempVoltage){
           if(i==0){ //If at highest voltage extreme...limit to highest voltage temp
             temperature = pgm_read_word_near(map_0);
             return temperature;
           }else{
             float voltRangeDiff = float(pgm_read_word_near(map_0 + (i-1)) - tempVoltage); //Difference between the voltage values bounding the ADC reading?
             float tempBias = (float(ADC_voltage-tempVoltage)/ voltRangeDiff); //Percentage of the ADC voltage between the lower and higher value. 100% = full upper, 0% = full lower
             temperature = pgm_read_word_near(degreesC + i) - byte(tempBias*5.0);
             return temperature;
           }
         }
      }
    break;
  }
}

void broadcast(){ //For debug only...not  planned for release
  //pass
}
