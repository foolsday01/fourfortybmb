#include "bmbTimer.h"

 bmbTimer::bmbTimer(int period){
   waitPeriod = period;
 }

void bmbTimer::start(){
   running = true;
   oldTime = millis();
 }

void bmbTimer::stop(){
  running = false;
}

 boolean bmbTimer::isItTime(){
   if(running){
     if((oldTime+waitPeriod) <= millis()){
       oldTime = millis();
       return true;
     }else{
       return false;
     }
   }else{
     return false;
   }
 }
