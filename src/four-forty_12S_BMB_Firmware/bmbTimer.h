#ifndef bmbTimer_h
#define bmbTimer_h

#include "Arduino.h"

class bmbTimer{

public:
  bmbTimer(int period);
  void start();
  void stop();
  boolean isItTime();

private:
  boolean running = false;
  int waitPeriod;
  unsigned long oldTime;
};

#endif
