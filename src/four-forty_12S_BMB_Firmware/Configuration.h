#ifndef Configuration_h
#define Configuration_h

//---EEPROM OVERWRITTEN PARAMETERS---
/*
The below variables will be overwritten by EEPROM values. Sample defaults kept below
*/
boolean autoBalance = false;               //Board will begin balancing whenever it detects UNACCEPTABLE_IMBALANCE
boolean I2C_enabled = true;                //Enables I2C interface
boolean serialEnabled = true;              //Enables serial interface
boolean balanceLED = false;                //Turns on iso-LED when balancing TODO
boolean enableAlertLine = true;            //Enable alert line output
boolean settingsModified = false;          //Remains false in EEPROM until some value updated (discourages leaving defaults)
//boolean balanceDuringDischarge = false;  //For future charge/discharge mode differentiation

byte boardSCount[8];                 //How many cells connected to this board
byte thermistorMap = 0;
byte enableExtTemp = 3;              //Enable alert monitoring of ext NTCs (0=none,1= only T1, 2= only T2, 3 = both)

//---THERMISTOR # mapping---
//0 = EPCOS 100K thermistor (EPCOS B57540G0104J000)

const PROGMEM byte degreesC[]  = { -25, -20, -15, -10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100};
const PROGMEM int map_0[] = {4652, 4543, 4410, 4250, 4062, 3848, 3605, 3343, 3066, 2782, 2500, 2226, 1965, 1723, 1502, 1304, 1128, 973, 838, 722, 622, 536, 462, 399, 345, 299};

int minExtTemp = -10;                //[Degrees C]
int maxExtTemp = 50;                 //[Degrees C]
int maxDieTemp = 60;                 //[Degrees C]
int warningDieTemp = 45;             //[Degrees C]

float maxVoltage = 4.20;             //Max permitted cell voltage [volts]
float minVoltage = 2.75;             //Min permitted cell voltage [volts]
float targetBalanceDifference;       //0.02  //Volts (balance will continue until target is reached)
float maxBalanceDifference;          //Volts (maximum allowable difference in brick voltages)

//---END OVERWRITTEN PARAMETERS---

//---FIXED PARAMETERS---
/*
Don't touch these unless you know what you're doing!
Reference the schematic and documentation for hints
*/

//Pin assignments
const byte contactor1Pin = 5;
const byte contactor2Pin = 6;
const byte ledPin = 7;
const byte chipSelectPin = 10;
const byte watchdogPin = 14;
const byte LTC_watchdogPin = 13;

//Hardcoded values
const char dieTempMin = -20;            //Don't allow operation below this value REVIEW: is this reasonable? TODO: convert to EEPROM
const byte I2C_address = 8;             //What I2C slave address should the primary board have?
const byte maxBoards = 8;               //58% @8 46%@4  //What's the max number of boards permitted to be connected?
const byte firmwareMajorVersion = 0;
const byte firmwareMinorVersion = 5;
const byte endMarker1 = '\n';           //End marker for serial interface
const byte endMarker2 = '\r';           //End marker for serial interface
const byte maxChars = 30;               //Defines longest serial message that can be recieved
const int balancePeriod = 5000;         //[ms] - how freq will it switch between cells that need balancing? (TODO: convert to EEPROM param eventually)


//---END FIXED PARAMETERS---

//---GLOBAL VARIABLES---
boolean foundBoards[16];          //Empty array to hold board address info (supports 16 board theoretical max)
boolean checkerBoard = true;      //Global boolean to swap between even and odd cells for balance
boolean LED_state = false;        //Global boolean to swap LED state
boolean broadcastOn = false;      //Broadcast some info (for debug)
boolean newData = false;          //Flag to indicate if new serial data is ready for parsing
boolean actuallyBalancing = false;//Describes if the board is attempting to balance
byte interfaceResponse[2];        //Bytes to return, 2 is the current max
byte validBytes = 0;              //Valid bytes in interfaceResponse
byte alertCode = 0;               //Holds highest priority alert when ALERT is true
byte masterMessage[3];            //Consider making local, holds I2C or serial input
char receivedChars[maxChars];     //Array to hold pre-parsed serial data

//unsigned long time = 0;       //Used for interacting with timer class TODO: remove once verified
boolean ALERT = false;
boolean BRICK_OVERVOLTAGE = false;
boolean BRICK_UNDERVOLTAGE = false;
boolean UNACCEPTABLE_IMBALANCE = false;
boolean NTC1_OVERTEMP = false;
boolean NTC1_UNDERTEMP = false;
boolean NTC2_OVERTEMP = false;
boolean NTC2_UNDERTEMP = false;
boolean DIE_OVERTEMP = false;
//boolean NTC1_CHARGE_OVERTEMP = false;
//boolean NTC1_CHARGE_UNDERTEMP = false;
//boolean NTC2_CHARGE_OVERTEMP = false;
//boolean NTC2_CHARGE_UNDERTEMP = false;

//---END FAULTS---

#endif
